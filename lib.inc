%define EXIT_CODE 60
%define WRITE_CODE 1
%define STDOUT_CODE 1
%define STDIN_CODE 0
%define READ_CODE 0

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov     rax, EXIT_CODE
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor     rax, rax
.loop:      
    cmp     byte [rdi+rax], `\0`
    je      .stop_loop
    inc     rax
    jmp     .loop
.stop_loop: 
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push    rdi
    call    string_length
    pop     rsi
    mov     rdx, rax
    mov     rax, WRITE_CODE
    mov     rdi, STDOUT_CODE
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push    rdi
    mov     rdi, rsp
    call    print_string
    pop     rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov     rdi, `\n`
    jmp     print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push    rbx
    mov     r8, rsp
    dec     rsp
    mov     byte [rsp], 0
    mov     rbx, 10
    mov     rax, rdi
.loop:
    xor     rdx, rdx
    div     rbx
    add     rdx, '0'
    dec     rsp
    mov     [rsp], dl
    cmp     rax, 0
    jne     .loop   
    mov     rdi, rsp
    push    r8
    call    print_string 
    pop     r8
    mov     rsp, r8
    pop     rbx
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test    rdi, rdi
    jns     .uprint
    push    rdi
    mov     rdi, '-'
    call    print_char
    pop     rdi
    neg     rdi
.uprint:
    jmp    print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push    rbx
    xor     rcx, rcx
.loop:
    mov     bl, [rdi+rcx]
    cmp     bl, [rsi+rcx]
    jne     .neq
    cmp     bl, 0
    je      .eq
    inc     rcx
    jmp     .loop
.neq:
    xor     rax, rax
    jmp     .eq_return
.eq:
    mov     rax, 1
.eq_return:
    pop     rbx
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov     rax, READ_CODE
    mov     rdi, STDIN_CODE
    mov     rdx, 1          ; count
    dec     rsp
    mov     byte [rsp], 0   ;set zero as default
    mov     rsi, rsp
    syscall
    mov     al, [rsp]
    inc     rsp
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor     rdx, rdx
.loop:  
    push    rsi
    push    rdi
    push    rdx
    call    read_char
    pop     rdx
    pop     rdi
    pop     rsi

    cmp     rax, `\0`
    je      .read_word_end

    cmp     rax, ` `
    je      .is_begin
    cmp     rax, `\t`
    je      .is_begin
    cmp     rax, `\n`
    je      .is_begin
    mov     [rdi+rdx], al
    inc     rdx

    cmp     rdx, rsi
    je      .overflow
    jne     .loop
.is_begin:
    cmp     rdx, 0
    je      .loop
    jne     .read_word_end
.overflow:
    xor     rax, rax
    ret
.read_word_end:
    mov     rax, rdi
    mov     byte [rdi+rdx], 0
    ret
 
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    push    rbx
    mov     rbx, 10
    xor     rax, rax
    xor     rsi, rsi
    xor     rcx, rcx

.loop:
    mov     cl, [rdi+rsi]
    cmp     rcx, '0'
    jb      .end
    cmp     rcx, '9'
    ja      .end
    sub     rcx, '0'
    mul     rbx
    add     rax, rcx
    inc     rsi
    jmp     .loop

.end:
    mov     rdx, rsi
    pop     rbx
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor     rcx, rcx
    mov     cl, [rdi]
    cmp     rcx, '-'
    jne     .readuint
    inc     rdi
.readuint:
    push    rcx
    call    parse_uint
    pop     rcx

    cmp     rdx, 0
    je      .end
    cmp     rcx, '-'
    jne      .end

    inc     rdx
    neg     rax
.end:
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push    rdi
    push    rsi
    push    rdx
    call    string_length
    pop     rdx
    pop     rsi
    pop     rdi

    cmp     rdx, rax
    jle     .overflow

    push    rbx
.loop:
    mov     bl, [rdi]
    mov     [rsi], bl
    cmp     bl, 0
    je      .end
    inc     rdi
    inc     rsi
    jmp     .loop
.overflow:
    xor     rax, rax
    ret
.end:
    pop    rbx
    ret